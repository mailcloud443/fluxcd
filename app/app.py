from flask import Flask
app = Flask(__name__)

@app.route('/test')
def hello_geek():
    return '<h1>bye from Flask & Docker</h2>'


if __name__ == "__main__":
    app.run(debug=True)