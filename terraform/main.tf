# Provider and terraform block configuration
terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}
terraform {
  backend "http" {}
}
provider "digitalocean" {
  token = var.do_token
}

# Create the Kubernetes master node
resource "digitalocean_droplet" "k8s_master" {
  name     = "k8s-master"
  region   = var.region
  size     = var.droplet_size
  image    = "ubuntu-22-04-x64"
  ssh_keys = [var.ssh_key_id]
  tags     = ["k8s-cluster", "master"]

  provisioner "remote-exec" {
    inline = [
      # Update package index
      "sudo apt-get update",
      
      # Install MicroK8s using snap
      "sudo snap install microk8s --classic",
      
      # Wait for MicroK8s to be ready
      "sudo microk8s status --wait-ready",
      
      # Add current user to microk8s group
      "sudo usermod -a -G microk8s $USER",
      
      # Create .kube directory and set permissions
      "mkdir -p ~/.kube",
      "sudo chown -R $USER ~/.kube",
      
      # Generate and write kubeconfig
      "sudo microk8s config > ~/.kube/config",
      
      # Enable common addons
      "sudo microk8s enable dns storage ingress cert-manager",
      
      # Create alias for kubectl
      "echo 'alias kubectl=\"microk8s kubectl\"' >> ~/.bashrc",
      
      # Allow port 25000 for worker joins
      "sudo ufw allow 25000",
       # Apply the ClusterIssuer YAML
      "echo \"apiVersion: cert-manager.io/v1\nkind: ClusterIssuer\nmetadata:\n  name: letsencrypt-microk8s\nspec:\n  acme:\n    server: https://acme-v02.api.letsencrypt.org/directory\n    email: mail@cloud443.in\n    privateKeySecretRef:\n      name: letsencrypt-microk8s\n    solvers:\n      - http01:\n          ingress:\n            class: public\" | sudo microk8s kubectl apply -f -",
      
      # Verify master node is ready
      "until sudo microk8s status | grep 'microk8s is running'; do sleep 10; done",
      "echo 'Master node is ready!'"
    ]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = file("/tmp/id_rsa")
      host        = self.ipv4_address
    }
  }
}

# Get join command after master is confirmed ready
resource "null_resource" "get_join_command" {
  depends_on = [digitalocean_droplet.k8s_master]

  provisioner "remote-exec" {
    inline = [
      "sleep 30", # Give additional time for services to stabilize
      "microk8s add-node | grep 'microk8s join' | head -n1 > /root/join_command.txt",
      # Verify join command was generated
      "if [ ! -s /root/join_command.txt ]; then echo 'Failed to generate join command' && exit 1; fi",
      "echo 'Join command generated successfully!'"
    ]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = file("/tmp/id_rsa")
      host        = digitalocean_droplet.k8s_master.ipv4_address
    }
  }
}

# Worker node
resource "digitalocean_droplet" "k8s_worker" {
  count    = var.worker_node_count
  name     = "k8s-worker-${count.index + 1}"
  region   = var.region
  size     = var.droplet_size
  image    = "ubuntu-22-04-x64"
  ssh_keys = [var.ssh_key_id]
  tags     = ["k8s-cluster", "worker"]

 provisioner "file" {
    source      = "/tmp/id_rsa" # Adjust the path to your private key
    destination = "/root/.ssh/d.pem"      # Store it in root's .ssh folder
    connection {
      type        = "ssh"
      user        = "root"
      private_key = file("/tmp/id_rsa")
      host        = self.ipv4_address
    }
  }

  provisioner "remote-exec" {
    inline = [
      # Update package index
      "sudo apt-get update",
      "chmod 400 /root/.ssh/d.pem",
      # Install MicroK8s using snap
      "sudo snap install microk8s --classic",
      
      # Wait for MicroK8s to be ready
      "sudo microk8s status --wait-ready",
      
      # Add current user to microk8s group
      "sudo usermod -a -G microk8s $USER",
      
      # Create .kube directory
      "mkdir -p ~/.kube",
      
      # Set proper permissions
      "sudo chown -R $USER ~/.kube",
      
      # Get and execute join command
       "JOIN_CMD=$(ssh -i /root/.ssh/d.pem -o StrictHostKeyChecking=no root@${digitalocean_droplet.k8s_master.ipv4_address} 'microk8s add-node' | grep -oP 'microk8s join [^ ]*' | head -n 1)",
    
      "echo 'Retrieved join command: '$JOIN_CMD",
      "$JOIN_CMD",
      
      # Verify join was successful
      "until sudo microk8s status | grep 'microk8s is running'; do sleep 10; done",
      "echo 'Worker node ${count.index + 1} joined successfully!'"
    ]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = file("/tmp/id_rsa")
      host        = self.ipv4_address
    }
  }

  depends_on = [null_resource.get_join_command]
}

# Variables
variable "do_token" {
  description = "DigitalOcean API token"
  sensitive   = true
}

variable "region" {
  description = "DigitalOcean region"
  default     = "nyc1"
}

variable "droplet_size" {
  description = "Size of the droplets"
  default     = "s-2vcpu-2gb"
}

variable "ssh_key_id" {
  description = "ID of your SSH key in DigitalOcean"
}

variable "worker_node_count" {
  description = "Number of worker nodes"
  default     = 1
}

# Outputs
output "master_ip" {
  value = digitalocean_droplet.k8s_master.ipv4_address
}

output "worker_ips" {
  value = digitalocean_droplet.k8s_worker[*].ipv4_address
}